(function() {

window.App = {
	Models: {},
	Collections: {},
	Views: {}
};

window.template = function(id) {
	return _.template( $('#' + id).html() );
};

App.Models.Task = Backbone.Model.extend({
	 initialize: function(){
            console.log("Welcome to this world");
        }
});

App.Collections.Tasks = Backbone.Collection.extend({
	model: App.Models.Task,

	 initialize: function() {
       console.log("Collection Initialize");
      
    },

	comparator: function(task) {
		console.log("Collection Comparator"+task.get("priority"));
		return task.get("priority");
	}
});
App.Views.Tasks = Backbone.View.extend({
	tagName: 'ul',

	initialize: function() {
		console.log('Tasks View Initialize');
		this.collection.on('add', this.render, this);
	
		CustomEvents.on('tasks:show', this.showTask, this);
	},

	showTask: function(id) {
		console.log("Views:Task: Show task event with id "+id );
	},

	render: function() {
		console.log('Tasks Render');
		
		$('ul').empty();
		this.collection.each(this.addOne, this);

		return this;
	},

	addOne: function(task) {
		var taskView = new App.Views.Task({ model: task });
		
		
		console.log('Add One');
		this.$el.append(taskView.render().el);

		

	}
});

App.Views.Task = Backbone.View.extend({
	tagName: 'li',

	template: template('taskTemplate'),

	initialize: function() {
		this.model.on('change', this.render, this);
		this.model.on('destroy', this.remove, this);
	},

	events: {
		'click .edit': 'editTask',
		'click .delete': 'destroy'
	},

	
	editTask: function() {
		var newTaskTitle = prompt('What would you like to change the text to?', this.model.get('title'));

		if ( !newTaskTitle ) return;
		this.model.set('title', newTaskTitle);
	},

	destroy: function() {
		this.model.destroy();
	},

	remove: function() {
		this.$el.remove();
	},

	render: function() {
		console.log('Task Render');
		var template = this.template( this.model.toJSON() );

		this.$el.html(template);

		return this;
	}
});


App.Views.AddTask = Backbone.View.extend({
	el: '#addTask',

	events: {
		'submit': 'submit',
		'click #sort':'sort'
	},


	sort: function(e){
		e.preventDefault();
		console.log('sort');
		CustomEvents.trigger("sort");
	},
	submit: function(e) {
		e.preventDefault();

		var newTaskTitle = $(e.currentTarget).find('input[name=task]').val();
		var priority=$(e.currentTarget).find('input[name=priority]').val();;
		this.collection.add(new App.Models.Task({ title: newTaskTitle, priority: priority }));


	}
});



CustomEvents = _.extend({}, Backbone.Events);

App.Router = Backbone.Router.extend({
	routes: {
		'': 'index',
		'show/:id': 'show',
		'download/:id/*filename': 'download',
		'showtask/:id': 'showtask'
	},

	showtask: function(id) {
		console.log("Route show the specific task with id "+id);
		CustomEvents.trigger("tasks:show",id);
	},

	download: function(id) {
		console.log("Route: Download the id "+id);
	},

	index: function() {
		console.log("Route: index page");
	},

	show: function(id) {
		console.log("Route: show page with id "+id);
	}
})

 
var tasksCollection = new App.Collections.Tasks([
	{
		title: 'Go to the store',
		priority: 7
	},
	{
		title: 'Go to the mall',
		priority: 0
	},
	{
		title: 'Get to work',
		priority: 1
	}
]);

var addTaskView = new App.Views.AddTask({ collection: tasksCollection });

var tasksView = new App.Views.Tasks({ collection: tasksCollection });
$('.tasks').html(tasksView.render().el);


router = new App.Router();
Backbone.history.start();

})();

