(function(){


// Initialization of apps namespace
window.App = {};

// Initialization for classes namespace
window.App = {
	Models: {},
	Views: {},
	Collections: {}
};

// Creating a templates render function
window.template = function(id) {
	return _.template($('#'+id).html());
}

// Model for task object
App.Models.Task = Backbone.Model.extend({
	validate: function(attrs) {
		if (! attrs.title) {
			return 'test';
		}
	}
});

// Task View Object
App.Views.Task = Backbone.View.extend({
	tagName: 'li',

	template: template('taskTemplate'),

	events: {
		'click .edit': 'editTask',
		'click .delete': 'deleteTask'
	},

	initialize: function(){
		this.model.on('change',this.render,this);
		this.model.on('destroy',this.remove,this);
	},	

	editTask: function(){
		var newTaskTitle = prompt('change the value to?', this.model.get('title'));
		this.model.set('title',newTaskTitle);
	},

	deleteTask: function(){
		this.model.destroy();
	},

	remove: function(){
		this.$el.remove();
	},

	render: function(){
		//console.log(this.template(this.model.toJSON()));
		//this.$el.html(this.model.get('title'));
		this.$el.html(this.template(this.model.toJSON()));
		return this;	
	}
});

// 
App.Views.Tasks = Backbone.View.extend({
	tagName: 'ul',

	initialize: function() {
		this.collection.on("add",this.addOne,this);
	},	

	render: function() {
		this.collection.each(this.addOne, this);
	},


	addOne: function(task) {
		var taskView = new App.Views.Task({ model: task });

		this.$el.append(taskView.render().el);
	}
});

App.Views.AddTask = Backbone.View.extend({
	el: '#addTask',

	initialize: function() {

	},

	events: {
		'submit' : 'submit'
	},

	submit: function(e) {
		e.preventDefault();

		var newTaskTitle = $(e.currentTarget).find('input[type=text]').val();
		console.log(newTaskTitle);

		var task = new App.Models.Task({ title: newTaskTitle });
		this.collection.add(task);
	}
});

App.Collections.Tasks = Backbone.Collection.extend({
	model: App.Models.Task
})

var tasks = new App.Collections.Tasks([
{
	title: 'Test task1',
	priority: 1
},
{
	title: 'Test task1',
	priority: 3
},
{
	title: 'Test task1',
	priority: 2
}
]);


var tasksView = new App.Views.Tasks({ collection: tasks });
var newTaskCreator = new App.Views.AddTask({ collection: tasks });
tasksView.render();
$(document.body).append(tasksView.el);




})();